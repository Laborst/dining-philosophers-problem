import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Implementation uses a token system for solution of the problem of dining philosophers
 */
public class TokenSystemSolution {

    enum PhilosopherState {GetHungry, Eat, Ponder}  // philosopher can get hungry, eat and ponder

    static final int philosopherCount = 5;

    /* Lists to hold the philosophers and the forks.
      Philosophers are threads while forks are locks. */
    static ArrayList<Fork> forks = new ArrayList<Fork>();
    static ArrayList<Philosopher> philosophers = new ArrayList<Philosopher>();
    /*------------------------------------------------*/

    static class Fork {
        public static final int ON_TABLE = -1;
        static int instances = 0;
        public int id;  // fork id number
        public AtomicInteger holder = new AtomicInteger(ON_TABLE);  // id number of philosopher, who holds the fork

        Fork() {
            id = instances++;
        }
    }

    static class Philosopher extends Thread {
        int id;  // philosopher id number
        static int instances = 0;
        static final int maxWaitMs = 100;  // maximum time for sleep
        static AtomicInteger token = new AtomicInteger(0);
        AtomicBoolean end = new AtomicBoolean(false);
        static Random rand = new Random();

        PhilosopherState state = PhilosopherState.GetHungry;  //at first, philosopher is hungry

        /* Philosopher needs in two forks for eating. */
        Fork left;
        Fork right;
        /*--------------------------------------------*/
        int timesEaten = 0;  // how much times philosopher holds both forks

        Philosopher() {
            id = instances++;
            left = forks.get(id);
            right = forks.get((id + 1) % philosopherCount);
        }

        /* Method, which makes philosopher doing smth for a while. */
        void doingForRandTime() {
            try {
                sleep(rand.nextInt(maxWaitMs));
            } catch (InterruptedException ex) {
            }
        }
        /*-----------------------------------------*/

        /* Method of getting fork */
        void getFork(Fork fork) {
            while (true) {
                // If no someone, who hold the fork, the philosopher get this, else, he's thinking.
                if (fork.holder.get() == Fork.ON_TABLE) {
                    fork.holder.set(id);
                    return;
                } else {
                    doingForRandTime();
                }
            }
        }
        /*------------------------*/

        @Override
        public void run() {
            do {
                if (state == PhilosopherState.Ponder) {
                    state = PhilosopherState.GetHungry;
                } else {
                    if (token.get() == id) {  // if philosopher gets token
                        getFork(left);
                        getFork(right);             //  he gets two forks
                        token.set((id + 2) % philosopherCount);  // and gives his turn
                        state = PhilosopherState.Eat;
                        timesEaten++;
                        doingForRandTime();                        //  philosopher eats for a while
                        left.holder.set(Fork.ON_TABLE);
                        right.holder.set(Fork.ON_TABLE);
                        state = PhilosopherState.Ponder;   //  philosopher ponders for a while
                        doingForRandTime();
                    } else {
                        doingForRandTime();
                    }
                }
            } while (!end.get());
        }
    }

    static class SolutionStart {
        public static void solution() {
            long endTime = System.currentTimeMillis() + (15 * 1000);

            /*Philosophers and forks adding*/
            for (int i = 0; i < philosopherCount; i++) forks.add(new Fork());
            for (int i = 0; i < philosopherCount; i++)
                philosophers.add(new Philosopher());
            philosophers.forEach(Thread::start);
            /*-----------------------------*/

            /* Philosophers' meal */
            do {
                StringBuilder sb = new StringBuilder();


                sb.append("Philosophers states:\n");
                for (int i = 0; i < 5; i++) {
                    sb.append("P" + Integer.toString(i) + " - ");
                    sb.append(philosophers.get(i).state.toString());
                    sb.append("\n");

                }

                sb.append("\nForks states:\n");

                for (int i = 0; i < 5; i++) {
                    sb.append("F" + Integer.toString(i) + " - ");
                    int holder = forks.get(i).holder.get();
                    sb.append(holder == -1 ? "   " : String.format("P%d", holder));
                    sb.append("\n");


                }

                sb.append("\n-------\n");

                System.out.println(sb.toString());
                try {
                    Thread.sleep(1000);
                } catch (Exception ex) {
                }
            } while (System.currentTimeMillis() < endTime);
            /*--------------------*/

            for (Philosopher p : philosophers) {
                p.end.set(true);
            } // philosophers end of eating

            System.out.println("How much have eaten by philosophers:\n");
            for (Philosopher p : philosophers) {
                System.out.printf("P%d: ate %,d times\n",
                        p.id, p.timesEaten);
            }

        }

    }

}
