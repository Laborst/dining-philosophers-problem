/**
 * The solution of Dining philosophers problem
 */
public class PhilosophersMain {
    public static void main(String[] args) {
        TokenSystemSolution.SolutionStart sol = new TokenSystemSolution.SolutionStart();
        sol.solution();
    }
}
